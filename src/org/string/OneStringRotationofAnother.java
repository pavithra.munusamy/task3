package org.string;

import java.util.Scanner;

public class OneStringRotationofAnother {
	
	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		System.out.println("Please enter First String");
		String first = sc.nextLine();
		
		System.out.println("Please enter rotation of String");
		String rotation = sc.nextLine();
		
		if (checkRotation(first, rotation)) {
			System.out.println(first + " and " + rotation + " are rotation");
			
		}
		else
		{
			System.out.println("No rotation");
		}
		sc.close();
	}

	private static boolean checkRotation(String first, String rotation) {
		if (first.length()!=rotation.length()) {
			return false;	
		}
		
		String concatenated=first + rotation;
		
		if (concatenated.indexOf(rotation)!=-1) {
			return true;
		}
		return false;
		
	}
	
}
