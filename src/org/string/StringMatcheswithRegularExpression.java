package org.string;

public class StringMatcheswithRegularExpression {
	
	public static void main(String[] args) {
		String[] alphabets = { "", "12345", "@53678", "743789", "247954a", "abcdu", "dhdeg34" };
		
		for(String alphabet : alphabets) {
			System.out.println(" have " + alphabet + "Contains alphabetic words : " + alphabet.matches(".*[A-Za-z].*"));
			
		}
		
		String[] numbers = {"4245", "+4689", "4678fh"};
		for(String number : numbers) {
			System.out.println(" number " + number + " contains only 1-9 digits : " + number.matches(".*[1-9].*"));
		}
		
	}

}
