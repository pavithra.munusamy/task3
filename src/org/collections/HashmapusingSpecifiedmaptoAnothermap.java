package org.collections;

import java.util.HashMap;

public class HashmapusingSpecifiedmaptoAnothermap {

	public static void main(String[] args) {
		
		HashMap<Integer, String> hashmap1= new HashMap<Integer, String>();
		HashMap<Integer, String> hashmap2= new HashMap<Integer, String>();
		
		hashmap1.put(1, "Mobile");
		hashmap1.put(2, "Charger");
		hashmap1.put(3, "Adapter");
		System.out.println("\nvalues in firstmap:" +hashmap1);
		
		hashmap2.put(4, "Laptop");
		hashmap2.put(5, "Desktop");
		hashmap2.put(5, "Tablet");
		System.out.println("\nvalues in Secondmap:" +hashmap2);
		
		hashmap2.putAll(hashmap1);
		System.out.println("\nNow values in map:" +hashmap2);
		
	}
}
