package org.collections;

import java.util.Set;
import java.util.TreeMap;

public class TreemapKeyvalues {
	
	public static void main(String[] args) {
		TreeMap<String, String> treemap=new TreeMap<String, String>();
		
		treemap.put("A1", "Class10");
		treemap.put("A2", "Class10");
		treemap.put("A3", "Class10");
		treemap.put("A4", "Class10");
		
		Set<String>keys=treemap.keySet();
		for(String key: keys) {
			System.out.println(key);
		}
	}

}
