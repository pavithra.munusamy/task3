package org.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Shufflingelements {

	public static void main(String[] args) {
		List<String> list_String = new ArrayList<String>();
		
		list_String.add("one");
		list_String.add("Two");
		list_String.add("Three");
		list_String.add("Four");
		list_String.add("Five");
		System.out.println("List before shuffling:\n" +list_String);
		Collections.shuffle(list_String);
		System.out.println("List after shuffling:\n" +list_String);
	}
}
