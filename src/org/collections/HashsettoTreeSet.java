package org.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class HashsettoTreeSet {
	public static void main(String[] args) {
		HashSet<String> hashset=new HashSet<String>();
		
		hashset.add("Laptop");
		hashset.add("Charge");
		hashset.add("three");
		hashset.add("Mobile");
		hashset.add("Scale");
		System.out.println("Pencil: " +hashset);
		
		Set<String> treeset=new TreeSet<String>(hashset);
		System.out.println("Treeset: ");
		for(String element : treeset) {
			System.out.println(element);
		}
		
	}

}
